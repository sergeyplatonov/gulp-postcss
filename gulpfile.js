var gulp             = require('gulp'),
    postcss          = require('gulp-postcss'),
    autoprefixer     = require('autoprefixer'),
    assets           = require('postcss-assets'),
    nested           = require('postcss-nested'),
    postcss_vars     = require('postcss-simple-vars'),
    import_part      = require('postcss-partial-import'),
    postcss_mixins   = require('postcss-mixins'),
    stylehacks       = require('stylehacks'),
    mqpacker         = require('css-mqpacker'),
    stylelint        = require('stylelint'),
    reporter         = require('postcss-browser-reporter'),
    pug              = require('gulp-pug'),
    browserSync      = require('browser-sync'),
    imageMin         = require('gulp-imagemin'),
    pngquant         = require('imagemin-pngquant'),
    cache            = require('gulp-cache'),
    rename           = require('gulp-rename'),
    cssnano          = require('gulp-cssnano'),
    concat           = require('gulp-concat')
    uglify           = require('gulp-uglify');
var config           = require('stylelint-config-standard');

// Jade пакет
gulp.task('pug', function() {
  return gulp.src('app/jade/**/*.jade')
  .pipe(pug({
    pretty: true,
		livereload: true
  }))
  .pipe(gulp.dest('app/'))
  .pipe(browserSync.reload({stream: true}))
});
// PostCss пакет
gulp.task('postcss', function(){
  var plugins = [
    stylelint(config),
    autoprefixer({browsers: ['last 2 version']}),
    nested,
    postcss_vars,
    import_part,
    mqpacker,
    postcss_mixins,
    stylehacks,
    reporter({
      'selector': 'body:before'
    })
  ];
  return gulp.src('app/postcss/**/*.css')
  .pipe(postcss(plugins))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({stream: true}))
});
// Browser-sync
gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		}
	});
});

gulp.task('scripts', function() {
  return gulp.src([ //Список всех необходимых файлов
    'app/libs/jquery/dist/jquery.min.js'
  ])
  .pipe(concat('libs.min.js'))
  .pipe(uglify()) //Сжимаем JS файл
  .pipe(gulp.dest('app/js'));
});

gulp.task('css-libs', function() {
  return gulp.src('app/css/libs.css')
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('app/css'));
});

gulp.task('watch', ['browser-sync', 'pug', 'postcss', 'css-libs', 'scripts'], function() {
  gulp.watch('app/jade/**/*.jade', ['pug']);
  gulp.watch('app/postcss/**/*.css', ['postcss']);
  gulp.watch('app/js/**/*.js', browserSync.reload);
	gulp.watch('app/*.html', browserSync.reload);
});

gulp.task('build', ['clean'], function(){
  var buildCss = gulp.src([
    'app/css/main.css',
    'app/css/libs.css'
  ])
  .pipe(gulp.dest('dist/css'));

  var buildFonts = gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'));

  var buildJs = gulp.src('app/js/**/*')
  .pipe(gulp.dest('dist/js'));

  var buildHtml = gulp.src()
  .pipe(gulp.dest('dist'));
});

gulp.task('clean', function() {
    return del.sync('dist');
});

gulp.task('img', function() {
	return gulp.src('app/img/**/*') // Берем все изображения из app
		.pipe(imageMin({  // Сжимаем их с наилучшими настройками с учетом кеширования
			interlaced: true,
			progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
		}))
		.pipe(gulp.dest('dist/img')); // Выгружаем на продакшен
});

gulp.task('default', ['watch']);
